package com.sp.poc;

import com.openshift.client.IOpenShiftConnection;
import com.openshift.client.OpenShiftConnectionFactory;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Project;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;


@SpringBootApplication
public class PocApplication {

    private static List<Report> details = new ArrayList<>();

    public static void main(String[] args) throws IOException, InterruptedException {
        SpringApplication.run(PocApplication.class, args);
        //runSeleniumTest();
        executeFromTerminal();
    }

    private static void runSeleniumTest() {
        System.setProperty("phantomjs.binary.path", "/home/sp/Desktop/phantomjs");
        System.setProperty("webdriver.gecko.driver", "/home/sp/Desktop/geckodriver");
        WebDriver driver = new PhantomJSDriver();
        //WebDriver driver = new FirefoxDriver();
        WebDriverWait wait = new WebDriverWait(driver, 3600);
        try {
            driver.get("https://google.com/ncr");
            driver.findElement(By.name("q")).sendKeys("cheese" + Keys.ENTER);
            WebElement firstResult = wait.until(presenceOfElementLocated(By.cssSelector("h3>div")));
            System.out.println(firstResult.getAttribute("textContent"));
        } finally {
            driver.quit();
        }
    }

    private static void connectToGit() throws GitLabApiException {
        GitLabApi gitLabApi = new GitLabApi(GitLabApi.ApiVersion.V4, "http://localhost:90", "82nJQBXveD3QduwCs85z");
        List<Project> projects = gitLabApi.getProjectApi().getOwnedProjects();
    }

    private static void connectToOpenshift() {
        IOpenShiftConnection connection = new OpenShiftConnectionFactory().getConnection("7078622", "sp-developer", "AzErTy1234");
        System.out.println(connection);
    }

    private static void executeFromTerminal() throws IOException, InterruptedException {
        String[] command = new String[]{"/bin/bash", "-c", "java -cp '.:/home/sp/Desktop/selenium.jar' /home/sp/Desktop/test.java", "with", "args"};
        Process proc = Runtime.getRuntime().exec(command);
        // Read the output
        BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
        String line = "";
        while ((line = reader.readLine()) != null) {
            System.out.print(line + "\n");
        }
        proc.waitFor();
    }

    public static void report(String actualValue, String expectedValue) {
        if (actualValue.equals(expectedValue)) {
            Report r = new Report("Pass", "Actual value '" + actualValue + "' matches expected value");
            details.add(r);
        } else {
            Report r = new Report("Fail", "Actual value '" + actualValue + "' does not match expected value '" + expectedValue + "'");
            details.add(r);
        }
    }

    public static void writeResults() {
        try {
            URI templatePath = new URI("/home/sp/Documents/Projects/poc/poc/src/main/java/com/sp/poc/report.html");
            String reportIn = new String(Files.readAllBytes(Paths.get(templatePath)));
            for (int i = 0; i < details.size(); i++) {
                reportIn = reportIn.replaceFirst("resultPlaceholder", "<tr><td>" + (i + 1) + "</td><td>" + details.get(i).getResult() + "</td><td>" + details.get(i).getResult() + "</td></tr>" + "resultPlaceholder");
            }

            String currentDate = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
            String reportPath = "Z:\\Documents\\Bas\\blog\\files\\report_" + currentDate + ".html";
            Files.write(Paths.get(reportPath), reportIn.getBytes(), StandardOpenOption.CREATE);

        } catch (Exception e) {
            System.out.println("Error when writing report file:\n" + e.toString());
        }
    }

}